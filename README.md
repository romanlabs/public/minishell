<div style="text-align: center;">
<img src="images/logo.png" alt="Demo" width="300">
</div>

# Table of Contents
- [Rlabs Mini Shell](#rlabs-mini-shell)
- [Installation](#installation)
- [Usage](#usage)
  - [Log level can be set for all uses of `Cmd`](#log-level-can-be-set-for-all-uses-of-cmd)
  - [Cmd returns the tuple `(stdout,stderr)`](#cmd-returns-the-tuple-stdoutstderr)
  - [`NonZeroExit` is raised for commands that return non-zero exit codes](#nonzeroexit-is-raised-for-commands-that-return-non-zero-exit-codes)
  - [Allows for use of `Cmd()` instead of `Cmd`, both usages work](#allows-for-use-of-cmd-instead-of-cmd-both-usages-work)
- [License](#license)
- [Copyright](#copyright)


# Rlabs Mini Shell

RLabs Mini Shell is a simple Python library for executing shell commands. Intended for quick prototyping.  

# Installation

```bash
pip3 install rlabs-mini-shell
```

# Usage

Sample code

```python
from rlabs_mini_shell.cmd import Cmd

Cmd.date()
Cmd.uname("-s")
Cmd.ipconfig.getifaddr.en0()
Cmd.ls(
    "-l",
    "tests/integration/ls_test_dir/file"
)
Cmd.cat(
    "tests/integration/ls_test_dir/file"
)
```

Output:

```bash
DEBUG    date                                                                                                       cmd.py:115
INFO     Sun  2 Jun 2024 00:21:42 PDT                                                                               cmd.py:128
                                                                                                                              
DEBUG    uname -s                                                                                                   cmd.py:115
INFO     Darwin                                                                                                     cmd.py:128
                                                                                                                              
DEBUG    ipconfig getifaddr en0                                                                                     cmd.py:115
INFO     192.168.1.205                                                                                              cmd.py:128
                                                                                                                              
DEBUG    ls -l tests/integration/ls_test_dir/file                                                                   cmd.py:115
INFO     -rw-r--r--@ 1 rromanotero  staff  11  1 Jun 23:28 tests/integration/ls_test_dir/file                       cmd.py:128
                                                                                                                              
DEBUG    cat tests/integration/ls_test_dir/file                                                                     cmd.py:115
INFO     i am a file    
```

#### Log level can be set for all uses of `Cmd`

Sample code:

```python
from rlabs_mini_shell.cmd import Cmd

Cmd.log_setup(
    logging.INFO
)
Cmd.date()
```

Output:

```bash
INFO     Sun  2 Jun 2024 00:08:12 PDT                                                                               cmd.py:146
```

#### Cmd returns the tuple `(stdout,stderr)` 

Sample code:

```bash
stdout, _ = Cmd.ipconfig.getifaddr.en0()
print("My IP Address is: ", stdout)
```

Output:

```bash
DEBUG    ipconfig getifaddr en0                                                                                     cmd.py:115
INFO     192.168.1.205                                                                                              cmd.py:128
                                                                                                                              
My IP Address is:  192.168.1.205
```

#### `NonZeroExit` is raised for commands that return non-zero exit codes

Sample code:

```python
from rlabs_mini_shell.cmd import Cmd

Cmd().invalid.command()
```

Output:

```bash
NonZeroExit: Command 'invalid command ' failed with return code 127:
/bin/sh: invalid: command not found
```

#### Allows for use of `Cmd()` instead of `Cmd`, both usages work

Sample code:

```python
Cmd.date()
Cmd().date()
```

Output:

```python
DEBUG    date                                                                                                       cmd.py:121
INFO     Sun  2 Jun 2024 00:34:02 PDT                                                                               cmd.py:134
                                                                                                                              
DEBUG    date                                                                                                       cmd.py:121
INFO     Sun  2 Jun 2024 00:34:02 PDT 
```

# License

This project is licensed under the GNU Lesser General Public License v3.0 - see the [LICENSE](./LICENSE) file for details.

# Copyright

Copyright (C) 2024 RomanLabs, Rafael Roman Otero