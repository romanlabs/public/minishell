# RLabs Mini Shell

RLabs Mini Shell is a simple Python library for executing shell commands. Intended for quick prototyping.  

[Project in Gitlab](https://gitlab.com/romanlabs/public/rlabs-mini-shell)

