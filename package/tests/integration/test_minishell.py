#
# Copyright (C) 2024 RomanLabs, Rafael Roman Otero
# This file is part of RLabs Mini Shell.
#
# RLabs Mini Shell is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# RLabs Mini Shell is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with RLabs Mini Shell. If not, see <http://www.gnu.org/licenses/>.
#
'''
    Test rlabs_mini_shell
'''
import pytest
import platform

from rlabs_mini_shell.cmd import Cmd
from rlabs_mini_shell.error import NonZeroExit
from rlabs_mini_shell.error import ArgumentError

TEST_FILE_PATH = "tests/integration/ls_test_dir/file"

def test_commands():
    '''
        Test Commands
    '''
    stdout, stderr = Cmd().echo("Hello, World!")
    assert "Hello, World!" == stdout
    assert not stderr

    stdout, stderr = Cmd().ls(
        "-l",
        TEST_FILE_PATH
    )
    assert TEST_FILE_PATH in stdout
    assert not stderr

    stdout, stderr = Cmd().cat(TEST_FILE_PATH)
    assert "i am a file" == stdout
    assert not stderr

    ## this tests for longer command chains
    os_type = platform.system()

    if os_type == "Darwin":
        # macOS
        stdout, stderr = Cmd.ipconfig.getifaddr.en0()
        assert "." in stdout
        assert not stderr
    elif os_type == "Linux":
        #
        # Linux
        #
        # There's nothing installed in the CI containers
        # that's long enough... so just let it fail with "ip not found"
        #
        with pytest.raises(NonZeroExit) as exc:
            stdout, stderr = Cmd.ip.addr.show.dev.en0()
            assert "not found" in str(exc)
    else:
        raise NotImplementedError(f"OS {os_type} is not supported")

def test_errors():
    '''
        Test Errors
    '''
    with pytest.raises(NonZeroExit) as exc:
        Cmd().ls(
            "-l",
            "I_DO_NOT_EXIST"
        )

        assert "No such file or directory" in str(exc)
        assert "ls" in str(exc)
        assert "sh" in str(exc)

    with pytest.raises(NonZeroExit) as exc:
        Cmd().invalid.command()

        assert "not found" in str(exc)
        assert "invalid" in str(exc)
        assert "sh" in str(exc)

    with pytest.raises(ArgumentError) as exc:
        Cmd().echo(
            argument="Hello, World!"
        )

        assert "keyword" in str(exc)
        assert "arguments" in str(exc)

